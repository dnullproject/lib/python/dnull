#!/bin/bash
set -e
cd ~/dNull/lib/python/dnull

PUB=${1:-dev}
OLD_VERSION=$(git describe --tags --abbrev=0)
OLD_MAJOR=$(echo $OLD_VERSION | awk 'BEGIN {FS=".";} { printf "%s.%s", $1,$2 }')
OLD_MINOR=$(echo $OLD_VERSION | awk 'BEGIN {FS=".";} { print $3 }')

export VERSION=$( echo "${OLD_MAJOR}.$(($OLD_MINOR+1))" )

echo "dnull - version:${VERSION} mode:${PUB}"
sleep 3

sed -i 's/^version=.*/version=\"'"${VERSION}"'\"/' setup.py

case ${PUB} in
  dev)
    python setup.py sdist
    pip install ./dist/dnull-${VERSION}.tar.gz
    ;;
  prod)
    git add setup.py && git commit -m "Release ${VERSION}"
    git tag -a ${VERSION} -m "Release ${VERSION}"

    python setup.py sdist

    twine upload dist/dnull-${VERSION}.tar.gz && pip install dnull==${VERSION}
    ;;
esac
